<?php

namespace Drupal\pruebas\src\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class ExampleController extends ControllerBase {

/**
 * {@inheritdoc}
 */
  public function build() {
    $build = [
      '#type' => 'markup',
      '#markup' => 'El modulico dice Hello World!',
    ];

  return $build;

  }

}
